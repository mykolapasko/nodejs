const express = require('express');
const fs = require('fs');
const path = require('path')

const port = process.env.PORT || 8080;
const app = express();


//Save requestes log
app.use((req, res, next) => {
  const now = new Date().toDateString()
  const log = `${now}: ${req.method} ${req.url}`
  console.log(log);
  fs.appendFile('logfile.log', log + '\n', (err) => {
    if (err) {
      console.log('Unable to append to server.log');
    }
  })
  next()
})


//Create files

app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

app.post('/createFile', function (req, res, next) {
  console.log(req.body)
  let dir = './tmp'

//   //Ensure ./tmp folder existence
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir)
  }

  //Write file
  fs.writeFileSync('./tmp/' + req.body.filename, req.body.content)

  
  res.json(req.body)
  // res.sendStatus(200)
  // next()
})
// app.post('/createFile', (req, res, next) => {
//   console.log('File creation ', req.body);
//   
// })

//Get file list
app.get('/getFiles', (req, res, next) => {
  let dir = './tmp'
  fs.readdirSync(dir, (err, files) => {
    res.send(files)
  }) 
})


app.listen(port, () => {
  console.log(`Server is now up on port ${port}`);
})

